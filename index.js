// console.log("hellowrold");

// [SECTION] Exponent Operator


// ES6 UPDATE
const firstNum = 8 ** 2
console.log(firstNum);

const secondNum = Math.pow(8,2);
console.log(secondNum);

// [SECTION] Template Literals
/*
	- Allows to write string withouth using the concatenation operator (+)
	- Greatly helps with code readability
*/

let name = "John";

// Pre-Template literal String
// Uses single quote or double quote
let message = "Hello " + name + "! Welcome to programming!";
console.log("Message without template literals: \n" + message);

// String Using template literals
// Uses backticks (````); // ES6 UPDATE
message = `Hello ${name}! Welcome to Programming!`;
console.log(`Message without template literals: \n${message}`);

const anotherMessage = `${name} attended a math competition.
He won it by solveing the problem 8 ** 2 with the solution of ${firstNum}`;
console.log(anotherMessage) ;

/*
	- Template literals allows us to write string with embedded JavaScript expressions
	- expressions are any valid unit of code that resolves to a value
	-"${}" are used to include JavaScript expressions in strings using template literals
*/

const interestRate = .1;
const principal = 100;

console.log(`The interest of your savings account is ${principal*interestRate}`);

// [SECTION] Array Destructuring

/*
	- Allows us to unpack elements in array into distinct variabls
	- Allows to to name array elements with variables, instead of using index numbers
	- Helps with code readiblity
	Syntax
		let/const [variableName, variable, variableName] = array;
*/

const fullName = ["Juan", "Dela", "Cruz"];

// Pre-Array destructuring
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);
console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to see you`);


// ES6 UPDATE
// Array Destructuring

const [firstName, middleName, lastName] = fullName;
console.log(firstName);
console.log(middleName);
console.log(lastName);

console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to see you`);

// [SECTION] Object Destructuring

/*
	- Allows us to unpack properties/keys of objects into distinct variables
	- Shortens the syntax for accessing properties from objects
	- Syntex
		let/const {propertyVariableName, propertyVariableName, propertyVariableName} = object;

	- Note - Rule:
		the variableName to be assigned in destructuring shoulde be the same as the propertyName

*/

const person = {
	givenName: "Jane",
	maidenName: "Dela",
	familyName: "Cruz"
}

// Pre-Object Destructing (using dot notation)
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's nice to see you`);

// Object Destructuring // ES6 Update

const {givenName, maidenName, familyName} = person;
console.log(givenName);
console.log(maidenName);
console.log(familyName);

console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's nice to see you`);

// [SECTION] Arrow Functions
/*
	- Compact alternative syntax to traditional functions
	- Useful for code snippets where creating functions will not be resued in any other portion of the code
	- Adheres to "DRY" (Don't Repeat Yourself) principle where there's no longer need to create a function and think of a nume of function that will only be used in certain code snippets

	Syntax -
		const variable = () => {
			console.log("");
		}
*/

const hello = () => {
	console.log("Hello world");
}

hello();

const printFullName = (firstName, middleInitial, lastName) => {
	console.log(`${firstName} ${middleInitial} ${lastName}`);
}

printFullName("John", "D", "Smith");
// ------------

console.log("------------");
const students = ["John", "Jane", "Judy"];

// Arrow Functions with loops
// Pre- Arrow Function
students.forEach(function(student){
	console.log(`${student} is a student.`);
})

// Arrow Function in forEach // ES6 Update
// The function is only used in the "forEach" method to print out a text with the student's names
students.forEach((student) => {
	console.log(`${student} is a student.`);
})


// [SECTION] Implicit Return Statement
/*
	- There are instances when you can omit the 'return' statement
	- This works because withouth the 'return' statement JavaScript implicitly adds it for the result of the function
*/

// Pre-Arrow function

/*const add = (numA, numB) => {
	return numA + numB;
}

let total = add(1,2);
console.log(total);*/

const add = (numA, numB) => numA + numB;

let total = add(1,2);
console.log(total);

// [SECTION] Default Argument Value
// Provides a default argument value if none is provided when the function is invoked

const greet = (name = 'User') => {
	return `Good morning, ${name}`
}

console.log(greet());

// [SECTION] - Class-Based Object Blueprints
/*
	- Allows creation/instantiation of objects using classes as blueprints
	Syntax -
		class className{
			constructor(objectPropertyA, objectPropertyB)
				this.objectPropertyA = objectPropertyB
				tihs.objectProperyB = objectPropertyB
		}

*/

class Car {
	contructor(brand, name, year){
		this.brand = brand
		this.name = name;
		this.year = year;
	}
}

// Instantiating an object

const myCar = new Car();
console.log(myCar);

myCar.brand = "Ford"
myCar.name = "Range Raptor"
myCar.year = 2021;
console.log(myCar);

const myNewCar = new Car("Toyota", "Vios", 2021);
console.log(myNewCar);